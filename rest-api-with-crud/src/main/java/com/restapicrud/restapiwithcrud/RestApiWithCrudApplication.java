package com.restapicrud.restapiwithcrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiWithCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiWithCrudApplication.class, args);
	}

}
