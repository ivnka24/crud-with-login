package com.restapicrud.restapiwithcrud.model;
import jakarta.persistence.*;
import java.util.Date;

@Entity
@Table(name = "images")
public class MImage extends BaseProperties{
    public Long getImage_id() {
        return image_id;
    }

    public void setImage_id(Long image_id) {
        this.image_id = image_id;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }

    public byte[] getImage_data() {
        return image_data;
    }

    public void setImage_data(byte[] image_data) {
        this.image_data = image_data;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long image_id;
    private String image_name;
    private byte[] image_data;
}
