package com.restapicrud.restapiwithcrud.model;

import jakarta.persistence.*;

import java.awt.*;
import java.util.Date;

@Entity
@Table(name = "users")
public class MUser extends BaseProperties{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long user_id;
    private String username;
    private String password;
    private String email;
    private String full_name;
    private Date date_of_birth;
    private String address;
    @OneToOne
    @JoinColumn(name = "profile_image_id")
    private MImage profileImage;

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public Date getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(Date date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public MImage getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(MImage profileImage) {
        this.profileImage = profileImage;
    }
}
