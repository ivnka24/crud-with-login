package com.restapicrud.restapiwithcrud.model;
import jakarta.persistence.*;
import java.util.Date;
@Entity
@Table(name = "transactions")
public class MTransaksi extends BaseProperties{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long transaction_id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private MUser mUser;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private MProduct mProduct;

    private Date transaction_date;
    private double amount;

    public Long getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(Long transaction_id) {
        this.transaction_id = transaction_id;
    }

    public MUser getmUser() {
        return mUser;
    }

    public void setmUser(MUser mUser) {
        this.mUser = mUser;
    }

    public MProduct getmProduct() {
        return mProduct;
    }

    public void setmProduct(MProduct mProduct) {
        this.mProduct = mProduct;
    }

    public Date getTransaction_date() {
        return transaction_date;
    }

    public void setTransaction_date(Date transaction_date) {
        this.transaction_date = transaction_date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
